import React, { useState } from "react";
import {
  View,
  TextInput,
  Button,
  StyleSheet,
  Text,
  TouchableOpacity,
  Image,


} from "react-native";
import { Picker } from "@react-native-picker/picker";
import { AntDesign } from "@expo/vector-icons";

//import DateTimePicket from "./components/datePicker";

const ReservasScreen = ({ navigation }) => {
  const [selectedCourt, setSelectedCourt] = useState("");
  const [reservationDate, setReservationDate] = useState(null); // Estado para a data da reserva
  const [startTime, setStartTime] = useState(""); // Estado para a hora de início
  const [endTime, setEndTime] = useState(""); // Estado para a hora de fim
  const [hourlyRate, setHourlyRate] = useState(0); // Estado para o valor por hora
  const [totalCost, setTotalCost] = useState(0); // Estado para o valor total
  const [courtImages, setCourtImages] = useState({
    Basquete: require('../assets/basquete.png'),
    Vôlei: require('../assets/tenis.png'),
    Futebol: require('../assets/futebol.png'),
    Tênis: require('../assets/volei.png'),
  });
  const [schedule, setSchedule] = useState({
    Date: 'Data de uso',
    timeStart: 'horario do uso',
    timeEnd: 'Fim do uso',
    Status_da_reserva: null,
  });




  const handleCourtSelection = (court) => {
    setSelectedCourt(court);
  };
  
  const selectedCourtImage = courtImages[selectedCourt];
  

  const handleReservation = () => {
    console.log("Data da reserva:", reservationDate); // Mostra a data da reserva
    console.log("Hora de início:", startTime); // Mostra a hora de início
    console.log("Hora de fim:", endTime); // Mostra a hora de fim
    console.log("Valor por hora:", hourlyRate);
    console.log("Valor total:", totalCost);
  };

  return (
    <View style={styles.container}>
      <Text style={styles.textInput}>INFORMAÇÕES SOBRE AS QUADRAS</Text>
      <View style={styles.courtContainer}>
      <Picker
      style={styles.picker}
      selectedValue={selectedCourt}
      onValueChange={(itemValue) => handleCourtSelection(itemValue)}
    >
      <Picker.Item label="Selecione a quadra" value="" />
      <Picker.Item label="Quadra de Basquete" value="Basquete" />
      <Picker.Item label="Quadra de Vôlei" value="Vôlei" />
      <Picker.Item label="Campo de Futebol" value="Futebol" />
      <Picker.Item label="Quadra de Tênis" value="Tênis" />
    </Picker>
  </View>

  {selectedCourt !== "" && (
    <>
      <Text style={styles.label}>Imagem da Quadra</Text>
      <Image
        source={selectedCourtImage}
        style={{ width: 200, height: 200 }}
      />
          <Text style={styles.label}>Data da Reserva</Text>
  <TextInput
    style={styles.input}
    placeholder="Data da Reserva"
    value={reservationDate}
    onChangeText={setReservationDate} // Atualiza o estado da data da reserva
  />
    <Text style={styles.label}>Hora de Início</Text>
  <TextInput
    style={styles.input}
    placeholder="Hora de Início"
    value={startTime}
    onChangeText={setStartTime} // Atualiza o estado da hora de início
  />
  <Text style={styles.label}>Hora de Fim</Text>
  <TextInput
    style={styles.input}
    placeholder="Hora de Fim"
    value={endTime}
    onChangeText={setEndTime} // Atualiza o estado da hora de fim
  />


  <Text style={styles.label}>Valor por Hora</Text>
<TextInput
  style={styles.input}
  placeholder="Valor por Hora"
  value={hourlyRate.toString()}
  onChangeText={(text) => setHourlyRate(parseFloat(text))}
/>

<Text style={styles.label}>Valor Total</Text>
<TextInput
  style={styles.input}
  placeholder="Valor Total"
  value={totalCost.toString()}
  onChangeText={(text) => setTotalCost(parseFloat(text))}
/>

        </>
      )}

      <View style={styles.buttonContainer}>
        <Button
          title="RESERVAR"
          onPress={handleReservation}
          color="#3e945a"
          disabled={selectedCourt === ""}

        />
      </View>

      <TouchableOpacity
        style={styles.backButton}
        onPress={() => navigation.navigate("Home")}
      >
        <AntDesign name="arrowleft" size={24} color="#3e945a" />
      </TouchableOpacity>

      <View style={styles.footer}>
        <Text style={styles.footerText}>
          © 2024 Reserva de Quadras Esportivas
        </Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#faf2f2",
    alignItems: "center",
    justifyContent: "center",
    paddingHorizontal: 16,
    marginLeft: 10,
    marginRight: 10,
  },
  textInput: {
    fontSize: 25,
    fontWeight: "bold",
    marginBottom: 500,
    textAlign: "center",
    marginVertical: 10,
  },
  label: {
    fontSize: 12,
    color: "#666",
    marginBottom: 5,
  },
  input: {
    borderColor: "#4dbf72",
    borderWidth: 3,
    borderRadius: 30,
    fontSize: 12,
    width: "80%",
    padding: 10,
    marginVertical: 3,
    marginTop: 3,
    marginBottom: 5,
    paddingHorizontal: 10,
    height: 40,
  },
  courtContainer: {
    borderRadius: 30,
    width: "80%",
    padding: 3,
    marginVertical: 10,
  },
  picker: {
    width: "100%",
    height: 40,
    color: "#333",
    fontSize: 12,
    borderRadius: 30,
    paddingHorizontal: 10,
    borderColor: "#4dbf72",
    borderWidth: 3,
  },
  buttonContainer: {
    backgroundColor: "#e6e6e6",
    borderRadius: 20,
    overflow: "hidden",
    width: "80%",
    marginBottom: 290,
    marginVertical: 10,
  },
  backButton: {
    position: "absolute",
    top: 40,
    left: 10,
    zIndex: 999,
  },
  footer: {
    position: "absolute",
    bottom: 0,
  },
  footerText: {
    fontSize: 12,
    color: "#666",
  },
});

export default ReservasScreen;
